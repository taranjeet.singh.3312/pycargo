class PyCargoException(Exception):
    pass


class InvalidHeaderException(PyCargoException):
    pass